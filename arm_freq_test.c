/*
 * An ARM CPU frequency test module. Creates the file
 * /sys/class/arm_freq_test/freq_test which can be read to test the CPU
 * frequency using the ARM cycle count register and getnstimeofday.
 *
 * Copyright (C) 2015 MaXentric Technologies, LLC
 *  Brandon Beresini <beresini@maxentric.com>
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/math64.h>
#include <linux/time.h>
#include <linux/uaccess.h>

/* Initialize the ARM cycle counters. */
inline void init_counters (int do_reset, int enable_divider) {
  // in general enable all counters (including cycle counter)
  int value = 1;

  // peform reset:
  if (do_reset) {
    value |= 2;     // reset all counters to zero.
    value |= 4;     // reset cycle counter to zero.
  }

  if (enable_divider)
    value |= 8;     // enable "by 64" divider for CCNT.

  value |= 16;

  // program the performance-counter control-register:
  asm volatile ("MCR p15, 0, %0, c9, c12, 0\t\n" :: "r"(value));

  // enable all counters:
  asm volatile ("MCR p15, 0, %0, c9, c12, 1\t\n" :: "r"(0x8000000f));

  // clear overflows:
  asm volatile ("MCR p15, 0, %0, c9, c12, 3\t\n" :: "r"(0x8000000f));
}

/* Read the ARM cycle count register */
inline unsigned int get_cyclecount (void) {
  unsigned int value;
  // Read CCNT Register
  asm volatile ("MRC p15, 0, %0, c9, c13, 0\t\n": "=r"(value));
  return value;
}

static ssize_t freq_test_show (struct class * class, struct class_attribute * attr, char * buf) {
  struct timespec start_time;
  volatile int test;
  int i;
  unsigned int start_ccnt, elapsed_ccnt[20];
  long long start_time_ns, elapsed_time_ns[20], estimated_freq[20];

  init_counters(1,0);

  // Get cycle count around getnstimeofday calls to reduce risk of underestimating
  // frequency at cost of potentially overestimating frequency.
  start_ccnt = get_cyclecount();
  getnstimeofday(&start_time);

  for (i = 0; i < 20; i++) {
    unsigned int stop_ccnt;
    long long stop_time_ns;
    struct timespec stop_time;

    for(test = 0; test < 1000000; test++); // Do null work.
		        
    getnstimeofday(&stop_time);
    stop_ccnt = get_cyclecount();

    start_time_ns = (long) start_time.tv_sec * 1000000000 + (long) start_time.tv_nsec;
    stop_time_ns = (long) stop_time.tv_sec * 1000000000 + (long) stop_time.tv_nsec;

    elapsed_ccnt[i] = stop_ccnt - start_ccnt;
    elapsed_time_ns[i] = stop_time_ns - start_time_ns;

    estimated_freq[i] = div64_s64((long long) elapsed_ccnt[i] * 1000, elapsed_time_ns[i]);
  }

  return sprintf(
                  buf,
                  "[ 0] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[ 1] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[ 2] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[ 3] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[ 4] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[ 5] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[ 6] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[ 7] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[ 8] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[ 9] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[10] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[11] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[12] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[13] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[14] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[15] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[16] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[17] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[18] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n"
                  "[19] Elapsed cycles: %u\tElapsed time: %lld\tEstimated Frequency: %lld MHz\n",
                  elapsed_ccnt[0], elapsed_time_ns[0], estimated_freq[0],
                  elapsed_ccnt[1], elapsed_time_ns[1], estimated_freq[1],
                  elapsed_ccnt[2], elapsed_time_ns[2], estimated_freq[2],
                  elapsed_ccnt[3], elapsed_time_ns[3], estimated_freq[3],
                  elapsed_ccnt[4], elapsed_time_ns[4], estimated_freq[4],
                  elapsed_ccnt[5], elapsed_time_ns[5], estimated_freq[5],
                  elapsed_ccnt[6], elapsed_time_ns[6], estimated_freq[6],
                  elapsed_ccnt[7], elapsed_time_ns[7], estimated_freq[7],
                  elapsed_ccnt[8], elapsed_time_ns[8], estimated_freq[8],
                  elapsed_ccnt[9], elapsed_time_ns[9], estimated_freq[9],
                  elapsed_ccnt[10], elapsed_time_ns[10], estimated_freq[10],
                  elapsed_ccnt[11], elapsed_time_ns[11], estimated_freq[11],
                  elapsed_ccnt[12], elapsed_time_ns[12], estimated_freq[12],
                  elapsed_ccnt[13], elapsed_time_ns[13], estimated_freq[13],
                  elapsed_ccnt[14], elapsed_time_ns[14], estimated_freq[14],
                  elapsed_ccnt[15], elapsed_time_ns[15], estimated_freq[15],
                  elapsed_ccnt[16], elapsed_time_ns[16], estimated_freq[16],
                  elapsed_ccnt[17], elapsed_time_ns[17], estimated_freq[17],
                  elapsed_ccnt[18], elapsed_time_ns[18], estimated_freq[18],
                  elapsed_ccnt[19], elapsed_time_ns[19], estimated_freq[19]
                );
}

static const CLASS_ATTR(freq_test, 0444, freq_test_show, NULL);

/*-------------------------------------------------------------------------*/

static struct class * arm_freq_test_class;

/*-------------------------------------------------------------------------*/

static int __init arm_freq_test_init (void) {
  arm_freq_test_class = class_create(THIS_MODULE, "arm_freq_test");
  if (IS_ERR(arm_freq_test_class)) {
    return PTR_ERR(arm_freq_test_class);
  }

  if (class_create_file(arm_freq_test_class, &class_attr_freq_test) < 0) {
    printk(KERN_ERR "arm_freq_test: Error: Failed to create sysfs entry.");
    class_destroy(arm_freq_test_class);
  }

  return 0;
}
module_init(arm_freq_test_init);

static void __exit arm_freq_test_exit (void) {
  class_remove_file(arm_freq_test_class, &class_attr_freq_test);
  class_destroy(arm_freq_test_class);
}
module_exit(arm_freq_test_exit);

MODULE_AUTHOR("Brandon Beresini, <beresini@maxentric.com>");
MODULE_DESCRIPTION("ARM cpu frequency test using ARM cycle counter and getnstimeofday.");
MODULE_LICENSE("GPL");
MODULE_ALIAS("arm_freq_test");
