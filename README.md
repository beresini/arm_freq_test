# README #

An ARM CPU frequency test module. Creates the file /sys/class/arm_freq_test/freq_test, which can be read to test the CPU frequency using the ARM cycle count register and getnstimeofday.

### SETUP ###

To build:

$ make

To install:

$ insmod arm_req_test.ko

### USE ###

To run the test, read the file /sys/class/arm_freq_test/freq_test.

$ cat /sys/class/arm_freq_test/freq_test

### CONTACT ###

Brandon Beresini <beresini@maxentric.com>